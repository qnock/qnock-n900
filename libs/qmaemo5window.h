#ifndef QMAEMO5WINDOW_H
#define QMAEMO5WINDOW_H

#include <QMainWindow>

class QMaemo5Window : public QMainWindow
{
    Q_OBJECT

public:
    QMaemo5Window(QWidget *parent = 0);
    virtual ~QMaemo5Window();

protected slots:
    void orientationChanged(QString);
    void toggleLandscape(bool);
    void toggleFullScreen(bool);

protected:
    void showEvent(QShowEvent *e);
    void hideEvent(QHideEvent *e);

    virtual void relayout();
};

#endif // QMAEMO5WINDOW_H
