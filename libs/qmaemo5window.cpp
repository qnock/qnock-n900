
#include <QDBusMessage>
#include <QDBusConnection>
#include <QActionGroup>
#include <QAction>
#include <QMenuBar>
#include <QApplication>
#include <QDesktopWidget>

#include <mce/mode-names.h>
#include <mce/dbus-names.h>

#include "qmaemo5window.h"

QMaemo5Window::QMaemo5Window(QWidget *parent) : QMainWindow(parent)
{
    setAttribute(Qt::WA_Maemo5StackedWindow);
/*
    bool isPortrait = (qApp->desktop()->screenGeometry().width() < qApp->desktop()->screenGeometry().height());

    QActionGroup *orientation = new QActionGroup(this);
    orientation->setExclusive(true);
    QAction *m_portrait = new QAction(tr("Portrait"), orientation);
    m_portrait->setCheckable(true);
    m_portrait->setChecked(isPortrait);
    QAction *m_landscape = new QAction(tr("Landscape"), orientation);
    m_landscape->setCheckable(true);
    m_landscape->setChecked(!isPortrait);
    QAction *m_fullscreen = new QAction(tr("Full screen"), this);
    m_fullscreen->setCheckable(true);

    connect(m_landscape, SIGNAL(toggled(bool)), this, SLOT(toggleLandscape(bool)));
    connect(m_fullscreen, SIGNAL(toggled(bool)), this, SLOT(toggleFullScreen(bool)));

    menuBar()->addActions(orientation->actions());
    menuBar()->addAction(m_fullscreen);
*/
}

QMaemo5Window::~QMaemo5Window()
{
}

void QMaemo5Window::toggleLandscape(bool b)
{
    if (b)
        setAttribute(Qt::WA_Maemo5ForceLandscapeOrientation, true);
    else
        setAttribute(Qt::WA_Maemo5ForcePortraitOrientation, true);

    relayout();
}

void QMaemo5Window::toggleFullScreen(bool b)
{
    setWindowState(b ? windowState() | Qt::WindowFullScreen
                     : windowState() & ~Qt::WindowFullScreen);
}

void QMaemo5Window::showEvent(QShowEvent *se)
{
    QDBusMessage reply = QDBusConnection::systemBus().call(QDBusMessage::createMethodCall(MCE_SERVICE, MCE_REQUEST_PATH,
                                                                     MCE_REQUEST_IF, MCE_ACCELEROMETER_ENABLE_REQ));
    if (reply.type() != QDBusMessage::ErrorMessage)
        orientationChanged(reply.arguments().value(0).toString());
    QDBusConnection::systemBus().connect("", MCE_SIGNAL_PATH, MCE_SIGNAL_IF,
                                         MCE_DEVICE_ORIENTATION_SIG, this, SLOT(orientationChanged(QString)));
    QMainWindow::showEvent(se);
}

void QMaemo5Window::hideEvent(QHideEvent *he)
{
    QDBusConnection::systemBus().disconnect("", MCE_SIGNAL_PATH, MCE_SIGNAL_IF,
                                            MCE_DEVICE_ORIENTATION_SIG, this, SLOT(orientationChanged(QString, QString, QString, int, int, int)));
    QDBusConnection::systemBus().call(QDBusMessage::createMethodCall(MCE_SERVICE, MCE_REQUEST_PATH,
                                                                     MCE_REQUEST_IF, MCE_ACCELEROMETER_DISABLE_REQ));
    QMainWindow::hideEvent(he);
}

void QMaemo5Window::orientationChanged(QString landscape)
{
    if (landscape == QLatin1String(MCE_ORIENTATION_LANDSCAPE))
        toggleLandscape(true);
    else if (landscape == QLatin1String(MCE_ORIENTATION_PORTRAIT))
        toggleLandscape(false);
}

void QMaemo5Window::relayout()
{
}
