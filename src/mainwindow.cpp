#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMaemo5Window(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Connect menu actions
    connect(ui->actionLoad, SIGNAL(triggered()), this, SLOT(slotLoad()));
    connect(ui->actionUnload, SIGNAL(triggered()), this, SLOT(slotUnload()));
    connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(slotSave()));
    connect(ui->actionQuit, SIGNAL(triggered()), this, SLOT(close()));
    connect(ui->actionAboutQt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(slotAbout()));

    // == MAEMO5 OPTIMISATIONS ==
    // Hiding some menu actions
    ui->actionQuit->setVisible(false);
    ui->actionAboutQt->setVisible(false);
    ui->actionAbout->setVisible(false);
    // Replacing the QComboBox
    ui->sequencesCB->hide();
    sequencesPB = new QMaemo5ValueButton(tr("Ports sequences"));
    sequencesPB->setValueLayout(QMaemo5ValueButton::ValueUnderText);
    selector = new QMaemo5ListPickSelector();
    selector->setModel(ui->sequencesCB->model());
    connect(selector, SIGNAL(selected(QString)), this, SLOT(slotSeqSelected(QString)));
    sequencesPB->setPickSelector(selector);
    QBoxLayout *bl = qobject_cast<QBoxLayout *>(ui->sequencesCB->parentWidget()->layout());
    bl->insertWidget(0, sequencesPB);


    // Connect buttons
    connect(ui->savePB, SIGNAL(clicked()), this, SLOT(slotSave()));
    connect(ui->sendPB, SIGNAL(clicked()), this, SLOT(slotSend()));

    connect(ui->toLE, SIGNAL(textChanged(const QString)), this, SLOT(slotInformationsModified()));
    connect(ui->portsPTE, SIGNAL(textChanged()), this, SLOT(slotInformationsModified()));
    connect(ui->sequencesCB, SIGNAL(activated(QString)), this, SLOT(slotSeqSelected(QString)));

    udpSocket = new QUdpSocket(this);
    currentFile = NULL;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::slotLoad()
{
    QTextStream in;

    // Choose a file
    QString fileName = QFileDialog::getOpenFileName(this, trUtf8("Qnock : Chose file"), QDir::homePath());
    if (fileName.isEmpty())
        return;
    else {
        // Check if an other file was loaded
        if (ui->actionUnload->isEnabled())
            slotUnload();
        currentFile = new QFile(fileName);
    }

    // Open the file
    if (!currentFile->open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox::critical(this, trUtf8("Qnock : Opening file"), trUtf8("Problem when openning the file"));
        return;
    }

    // Read sequences in the file
    QString name, to, tmp;
    QList<QString> sequences;

    in.setDevice(currentFile);
    while(!in.atEnd()) {
        tmp = in.readLine().trimmed();

        if (tmp.isEmpty()) continue;

        if (tmp.startsWith("Name :", Qt::CaseInsensitive))
            name = tmp.remove(0, 6).trimmed();
        else if (!name.isEmpty() && tmp.startsWith("Destination :", Qt::CaseInsensitive))
            to = tmp.remove(0, 13).simplified();
        else if (!name.isEmpty() && !to.isNull() && tmp.startsWith("Sequence :", Qt::CaseInsensitive)) {
            sequences = tmp.remove(0,10).simplified().split(' ');
            addSeqence(name, to, sequences);
        } else {
            name = QString();   // Can NOT be empty
            to.clear();         // Can be empty
            sequences.clear();  // Can be empty
        }
    }
    currentFile->close();

    ui->sequencesCB->setCurrentIndex(-1);
    ui->actionUnload->setEnabled(true);
}

void MainWindow::slotUnload()
{
    ui->sequencesCB->clear();
    ui->portsPTE->clear();
    ui->toLE->clear();
    portsList.clear();
    savedSeq.clear();
    delete currentFile;
    currentFile = NULL;

    ui->actionUnload->setEnabled(false);
}

void MainWindow::slotSave()
{
    bool ok;
    QString sequenceName, fileName;
    QTextStream out;

    // Check validity of informations
    if (!updatePortList())
        return;

    // Get the name for the sequence
    do {
        sequenceName = QInputDialog::getText(this, trUtf8("Qnock : Sequence name"), trUtf8("Enter a name to recognize your knock sequence :"), QLineEdit::Normal, "", &ok);
        if (!ok)
            return;
        else if (sequenceName.isEmpty())
            QMaemo5InformationBox::information ( this, trUtf8("<b>Invalid name :</b> Please specify a name for your knock sequence."), QMaemo5InformationBox::DefaultTimeout );
    } while (sequenceName.isEmpty()/* && QMessageBox::warning(this, trUtf8("Qnock : Invalid name"), trUtf8("Please specify a name for your knock sequence."))*/);

    // Ask which file to use if no file was loaded
    if (!currentFile) {
        fileName = QFileDialog::getSaveFileName(this, trUtf8("Qnock : Chose destination"), QDir::homePath());
        if (!fileName.isEmpty()) {
            currentFile = new QFile(fileName);
            if (currentFile->exists())
                slotLoad();
        }
        else
            return;
    }

    // Open the file in Append mode
    if (!currentFile->open(QIODevice::Append | QIODevice::Text)) {
        QMessageBox::critical(this, trUtf8("Qnock : Opening file"), trUtf8("Problem when openning/creating the file"));
        return;
    }

    Sequence seq;
    seq.name = sequenceName;
    seq.destination = ui->toLE->text().simplified();
    seq.ports = portsList;

    // Append the new sequence
    out.setDevice(currentFile);

    out << endl;
    out << QString("Name : ") << sequenceName << endl;
    out << QString("Destination : ") << seq.destination << endl;
    out << QString("Sequence : ");
    for (int i = 0; i < portsList.size(); ++i)
        out << QString("%1 ").arg(portsList.at(i));
    out << endl;

    currentFile->close();

    // Update comboBox, QHash, ...
    addSeqence(seq);
    ui->sequencesCB->setCurrentIndex(ui->sequencesCB->count() - 1);
    selector->setCurrentIndex(selector->model()->rowCount() - 1);

    ui->actionUnload->setEnabled(true);
}

void MainWindow::slotAbout()
{
}

void MainWindow::slotSend()
{
    QHostAddress toHA;

    if (!updatePortList())
        return;

    // Try to convert the destination text to IP address if it fail, trying to resolve the name
    if (!toHA.setAddress(ui->toLE->text())) {
        QHostInfo toHI = QHostInfo::fromName(ui->toLE->text());
        if (!toHI.addresses().isEmpty()) {      // May take a while to resolve the DNS name
            toHA = toHI.addresses().first();
        } else {            
//            QMessageBox::critical(this, trUtf8("Qnock : Bad destination"), trUtf8("Destination specified is not a valid IP and can't be resolved."));
            QMaemo5InformationBox::information ( this, trUtf8("<b>Destination</b> specified is not a valid IP or can't be resolved."), QMaemo5InformationBox::DefaultTimeout );
            ui->toLE->selectAll();
            ui->toLE->setFocus();
            return;
        }
    }

    for (int i = 0; i < portsList.size(); ++i)
        udpSocket->writeDatagram("", 0, toHA, portsList.at(i));
}

void MainWindow::slotInformationsModified()
{
    ui->sequencesCB->setCurrentIndex(-1);
    selector->setCurrentIndex(-1);
    // Bug, setting -1 shoold put no value text to the valueButton
    sequencesPB->setValueText("");
}

void MainWindow::slotSeqSelected(const QString& name)
{
    ui->toLE->blockSignals(true);
    ui->portsPTE->blockSignals(true);

    Sequence seq;

    ui->toLE->clear();
    ui->portsPTE->clear();

    if (savedSeq.contains(name)) {
        seq = savedSeq.value(name);
        ui->toLE->setText(seq.destination);
        for (int i = 0; i < seq.ports.size(); ++i)
            ui->portsPTE->appendPlainText(QString("%1").arg(seq.ports.at(i)));
    }

    updatePortList();

    ui->toLE->blockSignals(false);
    ui->portsPTE->blockSignals(false);
}


bool MainWindow::updatePortList()
{
    QStringList sl = ui->portsPTE->toPlainText().simplified().split(' ', QString::SkipEmptyParts);

    portsList.clear();

    for (int i = 0; i < sl.size(); ++i) {
        bool ok;
        int port;

        port = sl.at(i).toInt(&ok);
        if (ok && port >= 0 && port <= 65535)
            portsList.append((quint16)port);
        else {
//            QMessageBox::critical(this, trUtf8("Qnock : Bad port(s)"), trUtf8("One or more ports specified are not valid."));
            QMaemo5InformationBox::information ( this, trUtf8("<b>Bad port(s) :</b> One or more ports specified are not valid."), QMaemo5InformationBox::DefaultTimeout );
            portsList.clear();
            ui->portsPTE->setFocus();
            return FALSE;
        }
    }
    return TRUE;
}

void MainWindow::addSeqence(const Sequence &s)
{
    savedSeq.insert(s.name, s);
    ui->sequencesCB->addItem(s.name);
}

void MainWindow::addSeqence(const QString &name, const QString &to, const QList<QString> &ports)
{
    Sequence seq;

    seq.name = name;
    seq.destination = to;
    for (int i = 0; i < ports.size(); ++i)
        seq.ports.append((quint16)ports.at(i).toInt());

    savedSeq.insert(name, seq);
    ui->sequencesCB->addItem(name);
}

