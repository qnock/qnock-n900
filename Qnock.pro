# -------------------------------------------------
# Project created by QtCreator 2009-11-16T21:25:33
# -------------------------------------------------
QT += network dbus maemo5

TARGET      = Qnock
TEMPLATE    = app

MOC_DIR         = ./build/moc
OBJECTS_DIR     = ./build
UI_DIR          = ./build/ui
UI_HEADERS_DIR  = ./build/ui

SOURCES +=  src/main.cpp \
            src/mainwindow.cpp \
            libs/qmaemo5window.cpp
HEADERS +=  src/mainwindow.h \
            libs/qmaemo5window.h
FORMS +=    ui/mainwindow.ui
